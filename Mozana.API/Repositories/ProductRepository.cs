﻿using Microsoft.EntityFrameworkCore;
using Mozana.API.Data;
using Mozana.API.Entities;
using Mozana.API.Repositories.Contracts;

namespace Mozana.API.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly MozanaDbContext _dbContext;

        public ProductRepository(MozanaDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Product>> GetItems()
        {
            var products = await _dbContext.Products.ToListAsync();
            return products;
        }

        public async Task<IEnumerable<ProductCategory>> GetCategories()
        {
            var productCategories = await _dbContext.ProductCategories.ToListAsync();
            return productCategories;
        }

        public async Task<Product> GetItem(int id)
        {
            var product = await _dbContext.Products.FindAsync(id);
            return product;
        }

        public async Task<ProductCategory> GetCategory(int id)
        {
            var category = await _dbContext.ProductCategories.SingleOrDefaultAsync(c => c.Id == id);
            return category;
        }
    }
}
