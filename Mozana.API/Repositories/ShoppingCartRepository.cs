﻿using Microsoft.EntityFrameworkCore;
using Mozana.API.Data;
using Mozana.API.Entities;
using Mozana.API.Repositories.Contracts;
using Mozana.Models.Dtos;
#pragma warning disable CS8603

namespace Mozana.API.Repositories
{
    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly MozanaDbContext _mozanaDbContext;

        public ShoppingCartRepository(MozanaDbContext mozanaDbContext)
        {
            _mozanaDbContext = mozanaDbContext;
        }

        private async Task<bool> DoesCartItemExist(int cartId, int productId)
        {
            return await _mozanaDbContext.CartItems.AnyAsync(c => c.CartId == cartId && c.ProductId == productId);
        }

        public async Task<CartItem> AddItem(CartItemToAddDto cartItemToAddDto)
        {
            if (await DoesCartItemExist(cartItemToAddDto.CartId, cartItemToAddDto.ProductId))
                return null;

            var item = await _mozanaDbContext.Products.Where(product => product.Id == cartItemToAddDto.ProductId)
                .Select(product => new CartItem
                {
                    CartId = cartItemToAddDto.CartId,
                    ProductId = product.Id,
                    Quantity = cartItemToAddDto.Quantity
                }).SingleOrDefaultAsync();

            if (item == null)
                return null;

            var result = await _mozanaDbContext.CartItems.AddAsync(item);
            await _mozanaDbContext.SaveChangesAsync();
            return result.Entity;

        }

        public async Task<CartItem> UpdateQuantity(int id, CartItemQuantityUpdateDto cartItemQuantityUpdateDto)
        {
            var item = await _mozanaDbContext.CartItems.FindAsync(id);
            if (item != null)
            {
                item.Quantity = cartItemQuantityUpdateDto.Quantity;
                await _mozanaDbContext.SaveChangesAsync();
                return item;
            }
            return null;
        }

        public async Task<CartItem> DeleteItem(int id)
        {
            var item = await _mozanaDbContext.CartItems.FindAsync(id);

            if (item != null)
            {
                _mozanaDbContext.CartItems.Remove(item);
                await _mozanaDbContext.SaveChangesAsync();
            }

            return item;
        }

        public async Task<CartItem> GetItem(int id)
        {
            return await _mozanaDbContext.Carts
                .Join(_mozanaDbContext.CartItems, cart => cart.Id, cartItem => cartItem.CartId,
                    (cart, cartItem) => new { cart, cartItem })
                .Where(t => t.cartItem.Id == id)
                .Select(t => new CartItem
                {
                    Id = t.cartItem.Id,
                    ProductId = t.cartItem.ProductId,
                    Quantity = t.cartItem.Quantity,
                    CartId = t.cartItem.CartId,
                }).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<CartItem>> GetItems(int userId)
        {
            return await _mozanaDbContext.Carts
                .Join(_mozanaDbContext.CartItems, cart => cart.Id, cartItem => cartItem.CartId,
                    (cart, cartItem) => new { cart, cartItem })
                .Where(t => t.cart.UserId == userId)
                .Select(t => new CartItem
                {
                    Id = t.cartItem.Id,
                    ProductId = t.cartItem.ProductId,
                    Quantity = t.cartItem.Quantity,
                    CartId = t.cartItem.CartId
                }).ToListAsync();
        }
    }
}
