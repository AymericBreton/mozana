﻿using Mozana.API.Entities;
using Mozana.Models.Dtos;

namespace Mozana.API.Extensions
{
    public static class DtoConversions
    {
        public static IEnumerable<ProductDto> ConvertToDto(this IEnumerable<Product> products, IEnumerable<ProductCategory> productCategories)
        {
            return from product in products
                   join productCategory in productCategories
                       on product.CategoryId equals productCategory.Id
                   select new ProductDto()
                   {
                       Id = product.Id,
                       Name = product.Name,
                       Description = product.Description,
                       ImageUrl = product.ImageURL,
                       Price = product.Price,
                       Quantity = product.Quantity,
                       CategoryId = product.CategoryId,
                       CategoryName = productCategory.Name
                   };
        }

        public static ProductDto ConvertToDto(this Product product, ProductCategory productCategory)
        {
            return new ProductDto()
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                ImageUrl = product.ImageURL,
                Price = product.Price,
                Quantity = product.Quantity,
                CategoryId = product.CategoryId,
                CategoryName = productCategory.Name
            };
        }

        public static IEnumerable<CartItemDto> ConvertToDto(this IEnumerable<CartItem> cartItems,
            IEnumerable<Product> products)
        {
            return cartItems.Join(products, cartItem => cartItem.Id, product => product.Id,
                (cartItem, product) => new CartItemDto
                {
                    Id = cartItem.Id,
                    ProductId = cartItem.ProductId,
                    ProductName = product.Name,
                    ProductDescription = product.Description,
                    ProductImageUrl = product.ImageURL,
                    ProductPrice = product.Price,
                    CartId = cartItem.CartId,
                    Quantity = cartItem.Quantity,
                    TotalPrice = product.Price * cartItem.Quantity
                }).ToList();
        }

        public static CartItemDto ConvertToDto(this CartItem cartItem,
            Product product)
        {
            return new CartItemDto
            {
                Id = cartItem.Id,
                ProductId = cartItem.ProductId,
                ProductName = product.Name,
                ProductDescription = product.Description,
                ProductImageUrl = product.ImageURL,
                ProductPrice = product.Price,
                CartId = cartItem.CartId,
                Quantity = cartItem.Quantity,
                TotalPrice = product.Price * cartItem.Quantity
            };
        }
    }
}
