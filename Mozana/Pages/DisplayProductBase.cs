﻿using Microsoft.AspNetCore.Components;
using Mozana.Models.Dtos;

namespace Mozana.Pages
{
    public class DisplayProductBase : ComponentBase
    {
        [Parameter]
        public IEnumerable<ProductDto> Products { get; set; }
    }
}
